package analyzer;

import java.util.HashMap;

public class AnalysisResult {
    String firstColumnName;
    String secondColumnName;
    HashMap<String, String> table;

    public String getFirstColumnName() {
        return firstColumnName;
    }

    public String getSecondColumnName() {
        return secondColumnName;
    }

    public HashMap<String, String> getTable() {
        return table;
    }
}

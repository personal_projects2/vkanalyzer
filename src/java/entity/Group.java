package entity;

import javax.persistence.*;

@Entity
@Table(name = "groups")
public class Group {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int group_id;

    @Column
    int vkID;

    @Column
    String name;

    public int getId() {
        return group_id;
    }

    public void setId(int id) {
        this.group_id = id;
    }

    public int getVkID() {
        return vkID;
    }

    public void setVkID(int vkID) {
        this.vkID = vkID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

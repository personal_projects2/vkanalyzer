package entity;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int user_id;

    @Column
    int vkID;

    @Column
    String name;

    public int getId() {
        return user_id;
    }

    public void setId(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVkID() {
        return vkID;
    }

    public void setVkID(int vkID) {
        this.vkID = vkID;
    }
}
